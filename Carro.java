import um.Primeiro;

public class Carro {


    public static final String VERMELHO = "vermelho";
    public static final String PRETA = "preta";

    private Integer quantidadePneus;
    private Integer quantidadeCalotas;
    private Integer quantidadeParafusosPneu;
    private String cor;
    private Integer quantidadePortas;
    private Integer numeroChassi;
    private Integer anoFabricacao;
    private String combustivel;
    private  String marca;
    private String modelo;
    private String empresaAtomotiva;

    public Carro(Integer quantidadePneus) {
        setQuantidadePneus(quantidadePneus);
    }

    public Integer getQuantidadePneus() {
        return quantidadePneus + 1;
    }

    public void setEmpresaAtomotiva(String onebox){

        this.empresaAtomotiva = onebox;

    }

    public void setQuantidadePneus(Integer quantidadePneus) {
        setQuantidadeCalotas(quantidadePneus);
        quantidadePortas = quantidadePneus;
        quantidadeParafusosPneu = quantidadePneus * 4;
        this.quantidadePneus = quantidadePneus;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public Integer getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public Integer getNumeroChassi() {
        return numeroChassi;
    }

    public void setNumeroChassi(Integer numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public Integer getQuantidadePortas() {
        return quantidadePortas;
    }

    public void setQuantidadePortas(Integer quantidadePortas) {
        this.quantidadePortas = quantidadePortas;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Integer getQuantidadeCalotas() {
        return quantidadeCalotas;
    }

    public Integer getQuantidadeParafusosPneu() {
        return quantidadeParafusosPneu;
    }

    public void setQuantidadeParafusosPneu(Integer quantidadeParafusosPneu) {
        this.quantidadeParafusosPneu = quantidadeParafusosPneu;
    }

    public void setQuantidadeCalotas(Integer quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public void imprimeValores() {
        System.out.println("Marca " + getMarca());
        System.out.println("Modelo " + getModelo());
        System.out.println("Numero do chassi " + getNumeroChassi());
        System.out.println("Ano de fabricação " + getAnoFabricacao());
        System.out.println("Combustivel " + getCombustivel());
        System.out.println("Quantidade de portas " + getQuantidadePortas());
        System.out.println("Quantidade Pneus " + getQuantidadePneus());
        System.out.println("Quantidade de calotas " + getQuantidadeCalotas());
        System.out.println("Quantidade de parafusos pneus " + getQuantidadeParafusosPneu());
        System.out.println("Cor " + getCor());

        System.out.println(empresaAtomotiva);

    }
}
