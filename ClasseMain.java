import um.Primeiro;

public class ClasseMain {


    public static void  main(String... args) {
        Carro carro = new Carro(4);
        carro.setCor(Carro.VERMELHO);
        carro.setCor(Carro.PRETA);
        carro.setNumeroChassi(66485354);
        carro.setAnoFabricacao(2015);
        carro.setCombustivel("Gasoleo");
        carro.setMarca("Nissan");
        carro.setModelo("Juke");
        carro.setEmpresaAtomotiva("Empresa Onebox");

        carro.imprimeValores();

    }
}
