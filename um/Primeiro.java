package um;

public class Primeiro {

    private static Integer variavel = 1;

    public final static  Integer CONSTANTE = 10;

    private Integer escopoClass = 1;

    public static Integer metodoEstatico() {

        return variavel;
    }

    public void metodoPublico() {

        System.out.println(escopoClass);
        escopoClass = 2;

        System.out.println(escopoClass);
    }

    public void alteraVariavel() {
        System.out.println(escopoClass);
        escopoClass = escopoClass + 2;
        System.out.println(escopoClass);
    }
}